// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYGAME_MyGameGameMode_generated_h
#error "MyGameGameMode.generated.h already included, missing '#pragma once' in MyGameGameMode.h"
#endif
#define MYGAME_MyGameGameMode_generated_h

#define MyGame_Source_MyGame_MyGameGameMode_h_12_SPARSE_DATA
#define MyGame_Source_MyGame_MyGameGameMode_h_12_RPC_WRAPPERS
#define MyGame_Source_MyGame_MyGameGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MyGame_Source_MyGame_MyGameGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyGameGameMode(); \
	friend struct Z_Construct_UClass_AMyGameGameMode_Statics; \
public: \
	DECLARE_CLASS(AMyGameGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyGame"), MYGAME_API) \
	DECLARE_SERIALIZER(AMyGameGameMode)


#define MyGame_Source_MyGame_MyGameGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyGameGameMode(); \
	friend struct Z_Construct_UClass_AMyGameGameMode_Statics; \
public: \
	DECLARE_CLASS(AMyGameGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyGame"), MYGAME_API) \
	DECLARE_SERIALIZER(AMyGameGameMode)


#define MyGame_Source_MyGame_MyGameGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MYGAME_API AMyGameGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyGameGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYGAME_API, AMyGameGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGameGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYGAME_API AMyGameGameMode(AMyGameGameMode&&); \
	MYGAME_API AMyGameGameMode(const AMyGameGameMode&); \
public:


#define MyGame_Source_MyGame_MyGameGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYGAME_API AMyGameGameMode(AMyGameGameMode&&); \
	MYGAME_API AMyGameGameMode(const AMyGameGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYGAME_API, AMyGameGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGameGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyGameGameMode)


#define MyGame_Source_MyGame_MyGameGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define MyGame_Source_MyGame_MyGameGameMode_h_9_PROLOG
#define MyGame_Source_MyGame_MyGameGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyGame_Source_MyGame_MyGameGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MyGame_Source_MyGame_MyGameGameMode_h_12_SPARSE_DATA \
	MyGame_Source_MyGame_MyGameGameMode_h_12_RPC_WRAPPERS \
	MyGame_Source_MyGame_MyGameGameMode_h_12_INCLASS \
	MyGame_Source_MyGame_MyGameGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyGame_Source_MyGame_MyGameGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyGame_Source_MyGame_MyGameGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MyGame_Source_MyGame_MyGameGameMode_h_12_SPARSE_DATA \
	MyGame_Source_MyGame_MyGameGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyGame_Source_MyGame_MyGameGameMode_h_12_INCLASS_NO_PURE_DECLS \
	MyGame_Source_MyGame_MyGameGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYGAME_API UClass* StaticClass<class AMyGameGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyGame_Source_MyGame_MyGameGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
