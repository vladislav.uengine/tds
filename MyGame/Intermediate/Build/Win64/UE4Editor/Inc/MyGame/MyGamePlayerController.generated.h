// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYGAME_MyGamePlayerController_generated_h
#error "MyGamePlayerController.generated.h already included, missing '#pragma once' in MyGamePlayerController.h"
#endif
#define MYGAME_MyGamePlayerController_generated_h

#define MyGame_Source_MyGame_MyGamePlayerController_h_12_SPARSE_DATA
#define MyGame_Source_MyGame_MyGamePlayerController_h_12_RPC_WRAPPERS
#define MyGame_Source_MyGame_MyGamePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MyGame_Source_MyGame_MyGamePlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyGamePlayerController(); \
	friend struct Z_Construct_UClass_AMyGamePlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyGamePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyGame"), NO_API) \
	DECLARE_SERIALIZER(AMyGamePlayerController)


#define MyGame_Source_MyGame_MyGamePlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyGamePlayerController(); \
	friend struct Z_Construct_UClass_AMyGamePlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyGamePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyGame"), NO_API) \
	DECLARE_SERIALIZER(AMyGamePlayerController)


#define MyGame_Source_MyGame_MyGamePlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyGamePlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyGamePlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyGamePlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGamePlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyGamePlayerController(AMyGamePlayerController&&); \
	NO_API AMyGamePlayerController(const AMyGamePlayerController&); \
public:


#define MyGame_Source_MyGame_MyGamePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyGamePlayerController(AMyGamePlayerController&&); \
	NO_API AMyGamePlayerController(const AMyGamePlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyGamePlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGamePlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyGamePlayerController)


#define MyGame_Source_MyGame_MyGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define MyGame_Source_MyGame_MyGamePlayerController_h_9_PROLOG
#define MyGame_Source_MyGame_MyGamePlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_SPARSE_DATA \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_RPC_WRAPPERS \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_INCLASS \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyGame_Source_MyGame_MyGamePlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_SPARSE_DATA \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_INCLASS_NO_PURE_DECLS \
	MyGame_Source_MyGame_MyGamePlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYGAME_API UClass* StaticClass<class AMyGamePlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyGame_Source_MyGame_MyGamePlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
