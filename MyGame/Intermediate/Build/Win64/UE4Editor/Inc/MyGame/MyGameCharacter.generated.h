// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYGAME_MyGameCharacter_generated_h
#error "MyGameCharacter.generated.h already included, missing '#pragma once' in MyGameCharacter.h"
#endif
#define MYGAME_MyGameCharacter_generated_h

#define MyGame_Source_MyGame_MyGameCharacter_h_12_SPARSE_DATA
#define MyGame_Source_MyGame_MyGameCharacter_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMovementTick) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MovementTick(Z_Param_DeltaTime); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInputAxisX) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->InputAxisX(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInputAxisY) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->InputAxisY(Z_Param_Value); \
		P_NATIVE_END; \
	}


#define MyGame_Source_MyGame_MyGameCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMovementTick) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MovementTick(Z_Param_DeltaTime); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInputAxisX) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->InputAxisX(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInputAxisY) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->InputAxisY(Z_Param_Value); \
		P_NATIVE_END; \
	}


#define MyGame_Source_MyGame_MyGameCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyGameCharacter(); \
	friend struct Z_Construct_UClass_AMyGameCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyGame"), NO_API) \
	DECLARE_SERIALIZER(AMyGameCharacter)


#define MyGame_Source_MyGame_MyGameCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyGameCharacter(); \
	friend struct Z_Construct_UClass_AMyGameCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyGame"), NO_API) \
	DECLARE_SERIALIZER(AMyGameCharacter)


#define MyGame_Source_MyGame_MyGameCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyGameCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyGameCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyGameCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGameCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyGameCharacter(AMyGameCharacter&&); \
	NO_API AMyGameCharacter(const AMyGameCharacter&); \
public:


#define MyGame_Source_MyGame_MyGameCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyGameCharacter(AMyGameCharacter&&); \
	NO_API AMyGameCharacter(const AMyGameCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyGameCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGameCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyGameCharacter)


#define MyGame_Source_MyGame_MyGameCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(AMyGameCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMyGameCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(AMyGameCharacter, CursorToWorld); }


#define MyGame_Source_MyGame_MyGameCharacter_h_9_PROLOG
#define MyGame_Source_MyGame_MyGameCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyGame_Source_MyGame_MyGameCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	MyGame_Source_MyGame_MyGameCharacter_h_12_SPARSE_DATA \
	MyGame_Source_MyGame_MyGameCharacter_h_12_RPC_WRAPPERS \
	MyGame_Source_MyGame_MyGameCharacter_h_12_INCLASS \
	MyGame_Source_MyGame_MyGameCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyGame_Source_MyGame_MyGameCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyGame_Source_MyGame_MyGameCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	MyGame_Source_MyGame_MyGameCharacter_h_12_SPARSE_DATA \
	MyGame_Source_MyGame_MyGameCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyGame_Source_MyGame_MyGameCharacter_h_12_INCLASS_NO_PURE_DECLS \
	MyGame_Source_MyGame_MyGameCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYGAME_API UClass* StaticClass<class AMyGameCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyGame_Source_MyGame_MyGameCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
